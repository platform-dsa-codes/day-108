import java.util.Stack;

class Solution {
    public String simplifyPath(String path) {
        Stack<String> stack = new Stack<>();
        String[] components = path.split("/");

        for (String component : components) {
            if (component.equals("..")) {
                if (!stack.isEmpty()) {
                    stack.pop();
                }
            } else if (!component.equals(".") && !component.isEmpty()) {
                stack.push(component);
            }
        }

        StringBuilder result = new StringBuilder("/");
        for (String dir : stack) {
            result.append(dir).append("/");
        }

        // Remove trailing slash if not root directory
        if (result.length() > 1) {
            result.setLength(result.length() - 1);
        }

        return result.toString();
    }
}

// Test cases
class Main {
    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println(solution.simplifyPath("/home/")); // Output: "/home"
        System.out.println(solution.simplifyPath("/home//foo/")); // Output: "/home/foo"
        System.out.println(solution.simplifyPath("/home/user/Documents/../Pictures")); // Output: "/home/user/Pictures"
        System.out.println(solution.simplifyPath("/../")); // Output: "/"
        System.out.println(solution.simplifyPath("/.../a/../b/c/../d/./")); // Output: "/.../b/d"
    }
}
